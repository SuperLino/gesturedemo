package com.gdkdemo.gesture;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.TextView;

import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;

/**
 * 这是主activity
 * 
 * @author Zac
 * 
 */
public class MainActivity extends Activity {
	private GestureDetector mGestureDetector;
	private TextView tv_ScreenContent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// 隐藏标题栏
		setContentView(R.layout.activity_decoder);
		tv_ScreenContent = (TextView) findViewById(R.id.screen_content);
		mGestureDetector = createGestureDetector(this);

	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	private GestureDetector createGestureDetector(Context context) {
		GestureDetector gestureDetector = new GestureDetector(context);
		// Create a base listener for generic gestures
		gestureDetector.setBaseListener(new GestureDetector.BaseListener() {
			@Override
			public boolean onGesture(Gesture gesture) {
				if (gesture == Gesture.TAP) {
					tv_ScreenContent.setText(R.string.tap);
					return true;
				} else if (gesture == Gesture.TWO_TAP) {
					tv_ScreenContent.setText(R.string.two_tap);
					return true;
				} else if (gesture == Gesture.LONG_PRESS) {
					tv_ScreenContent.setText(R.string.long_press);
					return true;
				} else if (gesture == Gesture.SWIPE_LEFT) {
					tv_ScreenContent.setText(R.string.swipe_left);
					return true;
				} else if (gesture == Gesture.SWIPE_RIGHT) {
					tv_ScreenContent.setText(R.string.swipe_right);
					return true;
				}
				return false;

			}
		});
		gestureDetector.setFingerListener(new GestureDetector.FingerListener() {
			@Override
			public void onFingerCountChanged(int previousCount, int currentCount) {
				// do something on finger count changes
			}
		});
		gestureDetector.setScrollListener(new GestureDetector.ScrollListener() {
			@Override
			public boolean onScroll(float displacement, float delta,
					float velocity) {
				return false;
			}
		});
		return gestureDetector;
	}

	/*
	 * Send generic motion events to the gesture detector
	 */
	@Override
	public boolean onGenericMotionEvent(MotionEvent event) {
		if (mGestureDetector != null) {
			return mGestureDetector.onMotionEvent(event);
		}
		return false;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

}